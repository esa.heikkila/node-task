/**Basic arithmetic operations */
const mylib = {
    /**Multiline arrow function */
    add: (a,b)=>{
        const sum = a + b;
        return sum;
    },
    subtract: (a,b)=>{
        return a-b;
    },
    /**Sigleline arrow function*/
    divide: (a,b) =>{
        return a/b;
    },
    
   


    /**Regular function */
    multiply: function(a,b) {
        return a * b;
    }
    
};

module.exports=mylib;
